import {
  BrowserRouter as Router,
  Switch,
  Route,
//   Link
} from "react-router-dom";

import Home from "./Home";
import About from "./About";
import Template from "./Template";
import Login from "./Login";
import Profile from "./Profile";

// Navbar html component
export default function Navbar() {
  return (
    <Router>
      <nav className="navbar navbar-expand-sm bg-dark navbar-dark fixed-top">
        <div className="container-fluid">
          <a href="/" className="navbar-brand"><i className="fab fa-500px"></i> Saud's Application</a>
          <button type="button" className="navbar-toggler" data-bs-toggle="collapse" data-bs-target="#navbarCollapse">
            <span className="navbar-toggler-icon"></span>
          </button>
          <div className="collapse navbar-collapse" id="navbarCollapse">
            <div className="navbar-nav">
              <a href="/" className="nav-item nav-link active">Home</a>
              <a href="/templates" className="nav-item nav-link">Templates</a>
              <a href="/about" className="nav-item nav-link">About</a>
              <a href="/profile" className="nav-item nav-link">Profile</a>
            </div>
            <div className="navbar-nav ms-auto">
              <a href="/login" className="nav-item nav-link">Login</a>
            </div>
          </div>
        </div>
      </nav>
      <Switch>
        <Route exact path="/">
          <Home />
        </Route>
        <Route path="/about">
          <About />
        </Route>
        <Route path="/templates">
          <Template />
        </Route>
        <Route path="/login">
          <Login />
        </Route>
        <Route path="/profile">
          <Profile />
        </Route>
      </Switch>
    </Router>
  );
}