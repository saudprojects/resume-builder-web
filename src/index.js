import React from 'react';
import ReactDOM from 'react-dom';
import "jquery/dist/jquery.min.js";
import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap/dist/js/bootstrap.min.js';
import '@fortawesome/fontawesome-free/css/all.min.css';
import '@fortawesome/fontawesome-free/js/all.min.js';
import Navbar from "./components/Navbar";

ReactDOM.render(<Navbar />, document.getElementById('root'));
ReactDOM.render('Resume Builder - Home', document.getElementById('head-title'));
// ReactDOM.render(<App />, document.getElementById('root'));
